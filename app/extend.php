<?php



Blade::extend(function($view, $compiler)
{
    $pattern = $compiler->createMatcher('nospaces');

    return preg_replace($pattern, '$1<?php echo strtolower(preg_replace(\'/\\s+/\', \'\', $2)); ?>', $view);
     
});

?>