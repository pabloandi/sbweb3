<?php

class OPACController extends \BaseController {
        
    protected $layout='opac';
    protected $solr;


    function __construct()
    {
        // create a client instance
        $this->beforeFilter(function(){
            $this->solr = new \Solarium\Client(Config::get('solr'));
        });
        
    }
    
    

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
                return View::make('opac.index');
	}
        
        public function getConsultar(){
            $pagina=Input::get('page',1);
            $limite=Input::get('limite',10);
            
            $categoria=Input::get('categoria');
            $filtros=Input::get('filtros');
            $bibliotecas=Input::get('bibliotecas');
            $ordenar=Input::get('ordenar',array('score'=>'desc'));
            $q=Input::get('q');
            
            $parametros=array(
                    'filtros'         =>      $filtros,
                    'bibliotecas'     =>      $bibliotecas,
                    'categoria'       =>      $categoria,
                    'ordenar'         =>      $ordenar,
                    'limite'         =>       $limite,
                    'q'               =>      $q
            );
            
                        
            $fichas = Cache::remember('consulta:'.serialize(array_add($parametros,'page',$pagina)), 500, 
                    function() use ($parametros,$pagina){
                
                if(Config::get('sbweb.solr')){
                    extract($parametros);
                    $q=sprintf('%s:"%s"',$categoria,$q);
                    
                    if(Input::has('filtros')){
                    
                        foreach($filtros as $operador => $campovalor){
                            list($op,$c)=explode(':',$operador);
                            if(is_array($campovalor)){
                                
                                
                                foreach ($campovalor as $valor){
                                    
                                    $q.=sprintf(' %s %s:"%s"', $op,$c,$valor);
                                }
                                    
                            }
                            else{
                                list($op,$c)=explode(':',$operador);
                                $q.=sprintf(' %s %s:"%s"', $op,$c,$campovalor);
                                
                            }
                                
                        }
                    
                    }
                    
                    if(Input::has('bibliotecas')){
                        $q.=sprintf(" AND (%s)",  implode(" OR ", $bibliotecas));
                    }
                    
                    //die($q);
                    
                    $select=array(
                        'query' =>  $q,
                        'sort'  =>  $ordenar,
                        
                    );
                    
                    
                    $query = $this->solr->createSelect($select);
                    
                    if(Input::has('facetas')){
                        foreach(Input::get('facetas') as $faceta => $valor){
                            $query->createFilterQuery($faceta)->setQuery("$faceta:%T1%",array($valor));
                        }
                    }
                    
                    $facetSet=$query->getFacetSet(array(
                        'limit' =>  5,
                        'mincount' =>  1
                    ));
                    
                    $facetSet->createFacetField('tipomaterial')->setField('tipomaterial');
                    $facetSet->createFacetField('tipoficha')->setField('tipoficha');
                    $facetSet->createFacetField('biblioteca')->setField('biblioteca');
                    $facetSet->createFacetField('autor')->setField('autor');
                    $facetSet->createFacetField('editorial')->setField('editorial');
                    $facetSet->createFacetField('lugarpublicacion')->setField('lugarpublicacion');
                    $facetSet->createFacetField('idiomapublicacion')->setField('idiomapublicacion');
                    
                    $query->setStart(($pagina*$limite)-$limite);
                    $query->setRows($limite);
                    
                    return $this->solr->select($query);
                                      
                    
                }
                else {
                    
                    $fichas=DB::table('ficha')
                        ->join('etiquetasmarc','ficha.idficha'.'=','etiquetasmarc.ficha')
                         ->whereRaw('MATCH(etiquetamarc.contenido) AGAINST (?)',array($texto))
                        ->whereIn('ficha.tipomaterial',$categorias)
                        ->whereIn('ficha.biblioteca',$bibliotecas);
                    return $fichas;
                }
                    
                
            });
            
            
            
            if(Config::get('sbweb.solr')){
                
                $facetas=array(
                    'tipo material'  =>  $fichas->getFacetSet()->getFacet('tipomaterial'),
                    'tipo ficha'  =>  $fichas->getFacetSet()->getFacet('tipoficha'),
                    'biblioteca'  =>  $fichas->getFacetSet()->getFacet('biblioteca'),
                    'autor'  =>  $fichas->getFacetSet()->getFacet('autor'),
                    'editorial'  =>  $fichas->getFacetSet()->getFacet('editorial'),
                    'lugar publicacion'  =>  $fichas->getFacetSet()->getFacet('lugarpublicacion'),
                    'idioma publicacion'  =>  $fichas->getFacetSet()->getFacet('idiomapublicacion')
                );
                
                
                
                
                $pager=Paginator::make($fichas->getDocuments(),$fichas->getNumFound(),$limite);
                
                $pager->appends($parametros);
                
                
                $response=array(
                    'datos'  =>  View::make('opac.consulta',array('pager'=>$pager))->render(),
                    'facetas'   =>  View::make('opac.partials._facetas',array('facetas'=>$facetas))->render()
                );
                
            }
            else{
                
                $pager=Paginator::make(array_slice((array) $fichas,($pagina * $limite) - $limite, $limite, true),  $fichas->getNumFound(),$limite);
                
                $response=array(
                  'datos'    => View::make('opac.consulta',array('pager'=>$pager))
                );
            }
            
            return Response::json($response);
                
        }
        
        public function getBusquedaAvanzada(){
            
            $bibliotecas = Cache::remember('bibliotecas',500,function(){
                return DB::table('biblioteca')->where('activo',1)->get();
            });

            $tiposmaterial = Cache::remember('tiposmaterial',500,function(){
                return DB::table('tipomaterial')->get();
            });

            return View::make('opac.partials._busqueda_avanzada',array(
                'bibliotecas'   =>  $bibliotecas,
                'tiposmaterial'    =>  $tiposmaterial,
            ));
            
            
        }

        public function getMostrarFichaEjemplares(){
            if(Input::has("fichas")){
                
                //dd(Input::get('fichas'));
                
                $fichas=Ficha::whereIn('idficha',Input::get('fichas'))->get();
                
                //dd($fichas[0]->procesarEtiquetasmarc());
                
                return View::make('opac.fichaejemplares',array('fichas'=>$fichas));
                
            }
            else { App::abort(500,'No hay fichas que mostrar'); }
        }
        
        
        
        public function getContacto(){
            $bibliotecas = Cache::remember('bibliotecas',500,function(){
                    return DB::table('biblioteca')->where('activo',1)->get();
                });
                
            return View::make('opac.contacto',array('bibliotecas'=>$bibliotecas));
        }
        
        public function postEnviarCorreoContacto(){
            $mensaje=Input::get('mensaje_usuario_contacto');
            $nombre=Input::get('nombre_usuario_contacto');
            $correo=Input::get('correo_usuario_contacto');
            $asunto=Input::get('plan_usuario_contacto');
            
            $biblioteca=  Biblioteca::find(Input::get('biblioteca_contacto'));
            
            Mail::later(20,'emails.buzonsugerencias',array('nombre'=>$nombre, 'correo'=>$correo, 'asunto'=>$asunto, 'mensaje'=>$mensaje), function($message){
                            $message->to($biblioteca->correo, $biblioteca->nombre)->subject('Solicitud de reserva de material');
                        });
            
        }
        
	public function getAyuda(){
             return View::make('opac.ayuda');
        }
        
        public function getInfoUsuario(){
            $codigo=Input::get("codigo_usuario");
            
            try{
            
                $usuario=Usuario::with('laescuela','elgrupo')->where('no_cuenta','=',$codigo)->firstOrFail();
            }
            catch(Exception $e){
                 App::abort(500,'No existe el usuario'); 
            }
                //dd($usuario->laescuela->nombre);

                

            return View::make('opac.infousuario',array('usuario'=>$usuario));
           
            
           
             
        }
        
        public function getEjemplaresReservados(){
            $ejemplares_reservados=array();
            if(Session::has('ejemplares_reservados')){
                $ejemplares_reservados=Ejemplar::whereIn('idejemplar', array_unique(Session::get('ejemplares_reservados')))->get();
                //dd($ejemplares_reservados);
                /*
                foreach (Session::get('ejemplares_reservados') as $ejemplar){
                    //list($ficha_no,$no_adqui,$biblioteca)=explode("_",$ejemplar);
                    $ejemplares_reservados[]=  Ejemplar::find(explode("_",$ejemplar));
                }
                 * */
                 
            }
            
            return View::make('opac.ejemplaresreservados',array('ejemplares'=>$ejemplares_reservados));
        }
        
        public function getEnviarSolicitudReserva(){
            $usuario=Usuario::find(Input::get('reserva_codigo_usuario'));
            if(!$usuario)
                App::abort(500,'Usuario no existente');
            
            $ejemplares=  Ejemplar::find(explode("_",Input::get('ejemplares_a_reservar')));
            
            if(count($ejemplares)>0){
                $ejes=array();
                foreach ($ejemplares as $ejemplar){
                    $ejes[$ejemplar->biblioteca->correo][]=$ejemplar;
                }
                
                foreach(array_keys($ejes) as $biblioteca){
                    
                        Mail::later(20,'emails.solicitudreserva',array('usuario'=>$usuario, 'ejemplares'=>$ejes[$biblioteca]), function($message){
                            $message->to($biblioteca, $ejes[$biblioteca]->biblioteca->nombre)->subject('Solicitud de reserva de material');
                        });
                    
                }
            }
            else 
                App::abort(500,'No hay ejemplares que enviar');
            
        }
        
        
        public function getAgregarEjemplares(){
            $ejemplares=Input::get("ejemplar_solicitado");
            
            if(is_array($ejemplares)){
                foreach ($ejemplares as $ejemplar)
                    $ejemplares=Session::push('ejemplares_reservados',$ejemplar);
            }
            else 
                $ejemplares=Session::push('ejemplares_reservados',$ejemplares);
                
            
        }
        
        public function getRechazarEjemplar(){
            $ejemplar_rechazado=Input::get("ejemplar_rechazado");
            
            $ejemplares=Session::get('ejemplares_reservados');
            Session::put('ejemplares_reservados',  array_forget($ejemplares, $ejemplar_rechazado));
        }
        
        public function getTest(){
            //Session::flush();
            dd(Session::get('ejemplares_reservados'));
        }


}
