<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('multa', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

                        $table->integer('idmulta');
                        $table->string('no_cuenta',20);
                        $table->integer('escuela');
                        $table->text('observaciones')->nullable();
                        $table->date('fechaprestamo');
                        $table->date('fechaentrega');
                        $table->date('fechadevolucion');
                        $table->date('fechapago')->nullable();
                        $table->decimal('monto');

                        $table->primary('idmulta');
                        $table->foreign('no_cuenta')->references('no_cuenta')->on('usuario');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('multa'))
		Schema::drop('multa');
	}

}
