<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipomaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipomaterial', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

                        $table->integer('idtipomaterial');
                        $table->string('nombre',100);


                        $table->primary('idtipomaterial');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('tipomaterial'))
		Schema::drop('tipomaterial');
	}

}
