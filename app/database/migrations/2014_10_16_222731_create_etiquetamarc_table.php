<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtiquetamarcTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('etiquetamarc', function(Blueprint $table)
		{
			$table->engine = 'MyISAM';

                        $table->integer('idregistro');
                        $table->bigInteger('ficha');
                        $table->integer('numetiqueta');
                        $table->text('contenido')->nullable();
                        $table->integer('posicion')->nullable();
                        $table->integer('jerarquia')->nullable();


                        $table->primary('idregistro');
                        $table->foreign('ficha')->references('ficha')->on('idficha');
		});
                
                //DB::statement('alter table `etiquetamarc` add fulltext busqueda_etiquetamarc(contenido)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            if (Schema::hasTable('etiquetamarc'))
		Schema::drop('etiquetamarc');
	}

}
