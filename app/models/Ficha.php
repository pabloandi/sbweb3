<?php

class Ficha extends Eloquent{
    
    protected $table='ficha';
    
    public function ejemplares(){
        return $this->hasMany('Ejemplar','ficha','idficha');
    }
    
    public function etiquetasmarc(){
        return $this->hasMany('Etiquetamarc','ficha','idficha');
    }
    
    public function procesarEtiquetasmarc(){
        $etiquetas= self::etiquetasmarc()->get();
        $datos=array();
        $tipo=  $this->tipoficha;
        foreach ($etiquetas as $actual){
            $numero=(integer) $actual->numetiqueta;
            $contenido=$actual->contenido;
            switch ($numero){
                case 20:
                    switch ($tipo):
                        case "L":
                            $datos['ISBN'][]=$contenido;
                        case "R":
                            $datos['ISSN'][]=$contenido;
                    endswitch;
                    break;
                    
                case 40:
                    $datos['Fuente de catalogación'][]=$contenido;
                    break;
                    
                case 41:
                    $datos['Código de lengua'][]=$contenido;
                    break;
                    
                case 43:
                    $datos['Código de área geográfica'][]=$contenido;
                    break;
                    
                case 45:
                    $datos['Cronológico ó Fecha/Hora'][]=$contenido;
                    break;
                    
                case 50:
                    switch ($tipo):
                        case "R":
                            $datos['Signatura topográfica'][]=$contenido;
                        default:
                            $datos['Clasificación LC'][]=$contenido;
                    endswitch;
                    break;
                    
                case 82:
                    $datos['Clasificación Dewey'][]=$contenido;
                    break;
                    
                case 90:
                    $datos['Clasificación a nivel local'][]=$contenido;
                    break;
                    
                case 100:
                    $datos['Autor personal'][]=$contenido;
                    break;
                    
                case 110:
                    $datos['Autor corporativo'][]=$contenido;
                    break;
                    
                case 111:
                    $datos['Autor por asamblea, congreso, conferencia, etc.'][]=$contenido;
                    break;
                    
                case 201:
                    $datos['Título abreviado'][]=$contenido;
                    break;
                    
                case 240:
                    switch ($tipo):
                        case "A":
                            $datos['Volumen, Número, Páginas'][]=$contenido;
                        default:
                            $datos['Título Uniforme'][]=$contenido;
                    endswitch;
                    break;
                    
                case 243:
                    $datos['Título Uniforme Colectivo'][]=$contenido;
                    break;
                    
                case 245:
                    switch ($tipo):
                        case "R":
                            $datos['Título'][]=$contenido;
                        case "A":
                            $datos['Título del Artículo'][]=$contenido;
                        default:
                            $datos['Título/Mención de responsabilidad'][]=$contenido;
                    endswitch;
                    break;
                    
                case 246:
                    switch ($tipo):
                        case "R":
                            $datos['Título paralelo'][]=$contenido;
                        default:
                            $datos['Variante del Título'][]=$contenido;
                    endswitch;
                    break;
                    
                case 250:
                    $datos['Edición / Mención de responsabilidad'][]=$contenido;
                    break;
                    
                case 260:
                    switch ($tipo):
                        case "R":
                            $datos['Pie de imprenta'][]=$contenido;
                        default:
                            $datos['Lugar : Editorial'][]=$contenido;
                    endswitch;
                    break;
                    
                case 300:
                    switch ($tipo):
                        case "R":
                            $datos['Descripción física'][]=$contenido;
                        default:
                            $datos['Páginas o volumenes'][]=$contenido;
                    endswitch;
                    break;
                    
                case 306:
                    $datos['Duración del material'][]=$contenido;
                    break;
                    
                case 400:
                    $datos['Nota de serie bajo autor personal'][]=$contenido;
                    break;
                    
                case 410:
                    $datos['Nota de serie bajo autor corporativo'][]=$contenido;
                    break;
                    
                case 411:
                    $datos['Nota de serie bajo autor por asamblea, congreso, conferencia, etc.'][]=$contenido;
                    break;
                    
                case 440:
                    $datos['Serie'][]=$contenido;
                    break;
                    
                case 490:
                    $datos['Nota de serie'][]=$contenido;
                    break;
                    
                case 20:
                    $datos['Duración del material'][]=$contenido;
                    break;
                    
                case 500:
                    switch ($tipo):
                        case "R":
                            $datos['Notas'][]=$contenido;
                        default:
                            $datos['Notas generales'][]=$contenido;
                    endswitch;
                    break;
                    
                case 501:
                    $datos['Notas "Con"'][]=$contenido;
                    break;
                    
                case 502:
                    $datos['Nota de tesis'][]=$contenido;
                    break;
                    
                case 503:
                    $datos['Nota de historial bibliográfico'][]=$contenido;
                    break;
                    
                case 504:
                    $datos['Nota de bibliográfia'][]=$contenido;
                    break;
                    
                case 505:
                    $datos['Nota de contenido'][]=$contenido;
                    break;
                    
                case 506:
                    $datos['Nota de restricciones de acceso'][]=$contenido;
                    break;
                    
                case 508:
                    $datos['Notas de Crédito'][]=$contenido;
                    break;
                    
                case 510:
                    $datos['Nota de referencia/cita'][]=$contenido;
                    break;
                    
                case 511:
                    $datos['Notas de elenco'][]=$contenido;
                    break;
                    
                case 518:
                    $datos['Nota sobre fecha/hora y lugar de grabación'][]=$contenido;
                    break;
                    
                case 520:
                    switch ($tipo):
                        case "A":
                            $datos['Resumen'][]=$contenido;
                        default:
                            $datos['Nota de resumen'][]=$contenido;
                    endswitch;
                    break;
                    
                case 521:
                    $datos['Nota de audiencia'][]=$contenido;
                    break;
                    
                case 530:
                    $datos['Nota sobre otros formatos disponibles'][]=$contenido;
                    break;
                    
                case 534:
                    $datos['Nota de versión original'][]=$contenido;
                    break;
                    
                case 538:
                    $datos['Nota de detalle de sistema'][]=$contenido;
                    break;
                    
                case 546:
                    $datos['Nota de idioma'][]=$contenido;
                    break;
                    
                case 583:
                    $datos['Acción de conservación'][]=$contenido;
                    break;
                    
                case 590:
                    $datos['Nota de catalogación personalizada'][]=$contenido;
                    break;
                    
                case 600:
                    $datos['Encabezamiento bajo autor personal'][]=$contenido;
                    break;
                    
                case 610:
                    $datos['Encabezamiento bajo autor corporativo'][]=$contenido;
                    break;
                    
                case 611:
                    $datos['Encabezamiento bajo autor por asamblea, congreso, conferencia, etc.'][]=$contenido;
                    break;
                    
                case 630:
                    $datos['Encabezamiento bajo título uniforme'][]=$contenido;
                    break;
                    
                case 650:
                    switch ($tipo):
                        case "R":
                            $datos['Descriptores'][]=$contenido;
                        default:
                            $datos['Encabezamiento bajo temas generales'][]=$contenido;
                    endswitch;
                    break;
                    
                case 651:
                    $datos['Encabezamiento bajo nombres geográficos'][]=$contenido;
                    break;
                    
                case 700:
                    switch ($tipo):
                        case "R":
                            $datos['Fecha de inicio - terminación'][]=$contenido;
                        case "A":
                            $datos['Asientos secundarios'][]=$contenido;
                        default:
                            $datos['Asientos secundarios bajo autor personal'][]=$contenido;
                    endswitch;
                    break;
                    
                case 710:
                    $datos['Asientos secundarios bajo autor corporativo'][]=$contenido;
                    break;
                    
                case 711:
                    $datos['Asientos secundarios bajo autor por asamblea, congreso, conferencia, etc.'][]=$contenido;
                    break;
                    
                case 730:
                    $datos['Asientos secundarios bajo título uniforme'][]=$contenido;
                    break;
                    
                case 740:
                    $datos['Asientos secundarios de título (diferente de la portada)'][]=$contenido;
                    break;
                    
                case 760:
                    $datos['Fecha de fascículos no recibidos'][]=$contenido;
                    break;
                    
                case 780:
                    $datos['Acervo'][]=$contenido;
                    break;
                    
                case 856:
                    if(!preg_match('|^[a-zA-Z0-9]{3}$|i',trim($contenido)) && !preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', trim($contenido)))
					$descripcion=$contenido;
					
				if (preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', trim($contenido))) 
					$url=$contenido;
                
				
			    if(isset($url) && isset($descripcion)){
					$et="";
			   		   
					if($tipo=="A")
						$et="Recursos digitales";
					else
						$et="Líga a los recursos electrónicos";
				   
                    $datos[$et][]="<a href='$url' target='_blank'>$descripcion</a>";
				    unset($descripcion);
					unset($url);
				}
                    break;
                    
                case 999:
                    $datos['Detalles'][]=$contenido;
                    break;
                    
            }
        }
        
        $data=array();
        
        foreach($datos as $key => $value){
            switch ($key):
                        case "Título/Mención de responsabilidad": 
                        case "Título del Artículo": 
                        case "Título": 
                            $data[$key]=join('/ ',$value);
                            break;
                        
                        default :
                            $data[$key]=join($value,', ');
                            break;
                    endswitch;
        }
        
        
      
        return $data;
    }
}

?>
