<!DOCTYPE html>
<html>
    <head>
        <title>
            @section('title')
            SBWEB
            @show
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- CSS are placed here -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/opac.css" rel="stylesheet">
        <link href="css/offcanvas.css" rel="stylesheet">

        <style>
        @section('styles')
            
        @show
        </style>
    </head>

    <body>
        <!-- Container -->
        

            <!-- Content -->
            @yield('content')

        

        <!-- Scripts are placed here -->
        <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src='js/opac.js'></script>
        <script src='js/readmore.min.js'></script>
        <script src='js/offcanvas.js'></script>
        <script src='js/bootbox.min.js'></script>
    </body>
</html>