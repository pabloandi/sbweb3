{{ 
    Form::jqRemoteForm(array(
            'update'    =>  'respuesta_reserva',
            'url'       =>  URL::action('OPACController@getEjemplaresReservados'),
            'condition' =>  '$("#codigo_usuario_reserva").val()!="" && $("#usuario_existente").val()=="1"',
            'loading'   =>  'jQuery("#reserva-ejemplares-loader").show()',
            'success'   =>  'alert(data); $("#reserva_ejemplares").dialog("close")',
            'complete'  =>  '$("#reserva-ejemplares-loader").hide()',
            "method"    =>  "post"

    ), array('role'=>'form','id'=>'form-solreserva-ejemplares')) 
}}

        
    <div class="form-group">
        <label for="reserva-codigo-usuario">Código del usuario</label>
        <input type="text" class="form-control" id="reserva-codigo-usuario" name="reserva-codigo-usuario" placeholder="digite el número de cuenta"/>
    </div>
    @if(isset($ejemplares) && count($ejemplares)>0)
        
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class='panel-title'>Ejemplares para solicitar reserva</h4>
                </div>
                <ul class='list-group'>
                    @foreach($ejemplares as $ejemplar)
                        <li class="list-group-item ejemplar' id="ejemplar-reservado-{{{ $ejemplar->idejemplar }}}">
                            @include('opac.partials.ejemplares._ejemplar_reservado',array('ejemplar'=>$ejemplar))
                        </li>
                    @endforeach
                </ul>
            </div>
        
    @else
        No existen ejemplares reservados
    @endif

</form>
