<div class="panel-group" id="acordeon-ejemplares">
    
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Ejemplares asociados
                </h4>
            </div>
    
    
            <div class="list-group">

            @foreach($ejemplares as $ejemplar)
                    <a class='list-group-item' data-toggle="collapse" data-parent="#acordeon-ejemplares" href="#{{ $ejemplar->idejemplar }}">
                        {{{ $ejemplar->no_adqui }}} 
                        @if($ejemplar->estado()=='disponible' && !(preg_match("/^(r|R)+/", trim($ejemplar->laficha ? $ejemplar->laficha->clasificacion : ' '))) === true)
                            <span class="label label-success">Disponible</span>
                        @endif
                    </a>
                    
                    <div id='{{ $ejemplar->idejemplar }}' class='panel-collapse collapse'>
                        <div class='panel-body'>
                            @include('opac.partials.ejemplares._ejemplar_ficha', array('ejemplar'=>$ejemplar))
                        </div>
                    </div>

            @endforeach
            </div>
        
    </div>
</div>