
<li class="ficha list-group-item" id="{{ $ficha->id }}">
    <div class='row'>
        <div class='col-md-8'>
            <div class='media'>
            <div class='media-left hidden-xs'>
                    <a  href="#">
                      <img class="media-object img-thumbnail" src="https://openclipart.org/image/64px/svg_to_png/137011/1304760779.png" alt="..." title="Simple question sign by  boobaloo (/user-detail/boobaloo)" />
                    </a>
            </div>
            <div class="media-body">

              <h5 class="media-heading">
                  {{

                   Form::jqRemoteLink($ficha->titulo, array(
                      "url" => URL::action("OPACController@getMostrarFichaEjemplares"),
                      "update" => "consulta_fichas",
                      "script" => "true",
                      "loading" => "$('#enlace-ficha-".$ficha->id."').button('loading')",
                      "success" => "$('#enlace-ficha-".$ficha->id."').button('reset'); 
                                    bootbox.dialog({
                                        message: data,
                                        title: 'Vista de ficha y ejemplares asociados',
                                      });
                                    
                                    ",
                      "method" => "get",

                      "with" => "'fichas[]=".$ficha->id."'"
                          ), array(
                              "id" => "enlace-ficha-".$ficha->id,
                              "data-loading-text" => "Cargando...",
                              "data-toggle" => "tooltip",
                              "data-placement" => "left", 
                              "title" =>"Ver ficha completa y ejemplares"
                      ))
                  }}


              </h5>
                <strong class='small'>Por 
                    {{ $ficha->autor }}
                </strong>
                <div class="small">
                    <strong>Clasificación: </strong>
                    {{ $ficha->clasificacion }}
                </div>
                <div class="small">
                    <strong>ISBN: </strong>
                    {{ $ficha->isbn }}
                </div>
                <div class="small">
                    <strong>Fecha de publicación: </strong>
                    {{ $ficha->fechapublicacion }}
                </div>
                <div class="small">
                    <strong>Tipo: </strong>
                    {{ $ficha->tipomaterial }}
                </div>
                <div class="small">
                    <strong>Biblioteca: </strong>
                  {{ $ficha->biblioteca }}
                </div>


            </div>
        </div>
        </div>
        <div class='col-md-4'>
            <div>
                <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                <a href="#" onclick="jQuery('#enlace-ficha-{{ $ficha->id }}').click()">
                    Ver detalles de ficha
                </a>
            </div>
        </div>
    </div>
</li>