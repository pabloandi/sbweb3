<div class="panel panel-default">
    <div class="panel-heading">Detalles de ficha</div>
    <div class='panel-body'>
        
                @foreach($etiquetasmarc as $nombre => $contenido )
                <div class='row'>
                    <div class='col-md-4'>
                        <strong>{{{ $nombre }}}</strong>
                    </div>
                    <div class='col-md-8'>{{ $contenido }}</div>
                </div>
                @endforeach
        
    </div>
</div>