
    <div class="col-xs-12 col-sm-9">
        
        <div class="jumbotron">
            
                {{
        
                    Form::jqRemoteForm(array(
                        'update'    =>  'resultado-busqueda',
                        'url' => URL::Action('OPACController@getConsultar'),
                        'dataType'  =>  'json',
                        'loading' => 'jQuery("#boton-busqueda").button("loading")',
                        'success' => '
                        jQuery("#sidebar-mejorarbusqueda").html(data.facetas);
                        jQuery("#resultado-busqueda").html(data.datos);
                        jQuery("#boton-busqueda").button("reset"); 
                        jQuery("html,body").animate({scrollTop: $("#resultado-busqueda").offset().top},"slow");
                        ',
                        "method" => "get",
                        "with"  =>  "jQuery('#form_busqueda_principal').serialize()+'&'+jQuery('#busqueda-avanzada').serialize()"
                        ), 
                        array(
                            "id" => "form_busqueda_principal",
                            "class" => "navbar-form navbar-center",
                            "role" =>   "form"
                    ))

                }}
                
                
                

              <div class="form-group" >
                        <div class="input-group">
                            <input type="text" name="q" id="txtLIB"  class="form-control" placeholder="ej: Historia del arte" >
                            <div class="input-group-btn">
                                
                                    <button id="boton-busqueda" type="submit" class="btn btn-default" data-loading-text="<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>">
                                        <span class="glyphicon glyphicon-search "></span> 
                                        <span class='hidden-xs'>Buscar</span>
                                    </button>
                                    <a id="boton-busqueda-avanzada" class="btn btn-default" href="#" data-loading-text ="<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>"  onclick="{{ 
                                    
                                    Form::jqRemoteFunction( array(
                                        'url' => URL::action('OPACController@getBusquedaAvanzada'),
                                        'script' => 'true',
                                        'loading' => "jQuery('#boton-busqueda-avanzada').button('loading')",
                                        'success' => "jQuery('#boton-busqueda-avanzada').button('reset'); 
                                                      bootbox.dialog({
                                                        message: data,
                                                        closeButton: true,
                                                        title: 'Búsqueda avanzada',
                                                        buttons:{
                                                            main: {
                                                                label: 'Buscar',
                                                                className: 'btn-primary',
                                                                callback: function(){
                                                                    jQuery('#form-busqueda-avanzada').submit();
                                                                    this.remove();
                                                                }
                                                            }
                                                        }
                                                      });

                                                      ",
                                        'method' => 'get',
                                        ))
                                    
                                    
                                    }}">
                                        <span class="glyphicon glyphicon-filter"></span>
                                        <span class='hidden-xs'>Búsqueda avanzada</span>
                                    </a>
                                    <a class="btn btn-default" href="#" onclick="$('#txtLIB').val(''); location.reload();">
                                        <span class="glyphicon glyphicon-refresh"></span>
                                        <span class='hidden-xs'>Limpiar</span>
                                    </a>
                                    <div id='filtro_facetas'></div>

                                

                            </div>

                        </div>
                    </div>

                {{ Form::close() }}
            
        </div>
        
        <div id='resultado-busqueda' class='row pagina-ajax'></div>
        
    </div>

