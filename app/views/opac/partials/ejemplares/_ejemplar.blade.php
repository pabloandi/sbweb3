<div class='media'>
    <div class='media-left hidden-xs'>
        <a href="#">
            <img class="media-object img-thumbnail" src="https://openclipart.org/image/64px/svg_to_png/137011/1304760779.png" alt="..." title="Simple question sign by  boobaloo (/user-detail/boobaloo)" />
        </a>
    </div>
    <div class="media-body small">
        <h4 class="media-heading">
            {{{ $ejemplar->laficha->titulo }}}
        </h4>
        <div>
            <strong>
                Número de adquisición:
            </strong> 
            {{{ $ejemplar->no_adqui }}}
        </div>
        <div>
            <strong>
                Ficha: 
            </strong>
            {{{ $ejemplar->ficha_no }}}
        </div>
        <div>
            <strong>
                Biblioteca:
            </strong> 
            {{{ $ejemplar->labiblioteca ? $ejemplar->labiblioteca->nombre : 'Sin biblioteca' }}}
        </div>
        @if($ejemplar->laficha->tipoficha == "R")
            <div>
                <strong>
                    Año:
                </strong>
                {{{ $ejemplar->revanio }}}
            </div>
            <div>
                <strong>
                    Volumen:
                </strong>
                {{{ $ejemplar->revvol }}}
            </div>
            <div>
                <strong>
                    Número:
                </strong>
                {{{ $ejemplar->revnum }}}
            </div>
        @endif
        <div>
            <strong>
                Tipo de material:
            </strong>
            {{{ $ejemplar->eltipomaterial ? $ejemplar->eltipomaterial->nombre : 'Sin tipo de material' }}}
        </div>
        <div>
            <strong>
                Estado:
            </strong>
            {{{ $ejemplar->estado() }}}
        </div>
    </div>
</div>