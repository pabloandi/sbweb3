<div class='row'>
    <div class='col-md-8'>
        @include('opac.partials.ejemplares._ejemplar',array('ejemplar'=>$ejemplar))
    </div>
    <div class='col-md-4'>

            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>

           {{
                Form::jqRemoteLink('Rechazar este ejemplar',
                    array(
                        "url"       =>  URL::action("OPACController@getRechazarEjemplar"),
                        "update"    =>  "listado_ejemplares_reservados",
                        "loading"   =>  "jQuery('#rechazar-ejemplar-".$ejemplar->idejemplar."').button('loading')",
                        "script"    =>  'true',
                        "confirm"   =>  "Esta seguro de rechazar este ejemplar?",
                        "success"   =>  "
                                        jQuery('#rechazar-ejemplar-".$ejemplar->idejemplar."').button('loading');
                                        jQuery('#ejemplar-reservado-".$ejemplar->idejemplar."').remove();
                                        ",
                        "method"    =>  "get",
                        "with"      =>  "'ejemplar_rechazado='+".$ejemplar->idejemplar
                    ),
                    array(
                        "id"=>"rechazar-ejemplar-".$ejemplar->idejemplar
                        )
                )
           }}

           {{ Form::hidden('ejemplares_a_reservar[]',$ejemplar->idejemplar) }}
        </div>
</div>