<?php $funcion_contacto = Form::jqRemoteFunction( array(
            'url' => URL::action('OPACController@getContacto'),
            'script' => 'true',
            'loading' => "jQuery('#boton-contacto').button('loading')",
            'success' => "jQuery('#boton-contacto').button('reset'); 
                          bootbox.dialog({
                            message: data,
                            title: 'Formulario de contacto',
                            buttons:{
                                main: {
                                    label: 'Enviar',
                                    className: 'btn-primary',
                                    callback: function(){
                                        jQuery('#form-contacto-institucion').submit();
                                    }
                                }
                            }
                          });
                            
                          ",
            'method' => 'get',
            )); 
        
$funcion_ayuda=Form::jqRemoteFunction(array( 
    "url" => URL::action("OPACController@getAyuda"),
    "update" => "ayuda",
    "script" => 'true',
    "loading"=> "$('#boton-ayuda').button('loading')",
    "success" => "$('#boton-ayuda').button('reset'); 
                    bootbox.dialog({
                            message: data,
                            title: 'Ayuda',
                          });
                
                 ",
    "method" => "get",
        )); 

$funcion_carrito=Form::jqRemoteFunction( 
            array(
            "url" => URL::action("OPACController@getEjemplaresReservados"),
            "update" => array(
                'success' => "reserva_ejemplares",
                'failure' => "$('#boton-reserva').button('reset'); alert('no existen ejemplares solicitados');"
            ),
            "script" => 'true',
            "loading"=> "$('#boton-reserva').button('loading')",
            "success" => "$('#boton-reserva').button('reset'); 
                        bootbox.dialog({
                            message: data,
                            title: 'Ver ejemplares solicitados para reserva',
                          });
                        
                         ",
            "method" => "get"
         ));

?>


<ul class='list-inline'>
    <li>
        <a id ="boton-contacto" data-loading-text ="<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>" href="#" onclick="{{ $funcion_contacto }}" >
            <span class="glyphicon glyphicon-envelope"></span>
            <span class='hidden-xs'>Contacto</span>
        </a>
    </li>
    <li>
        <a id ="boton-contacto" data-loading-text ="<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>" href="#" onclick="{{ $funcion_ayuda }}" >
            <span class="glyphicon glyphicon-question-sign"></span>
            <span class='hidden-xs'>Ayuda</span>
        </a>
    </li>
    <li>
        <a id ="boton-reserva" data-loading-text ="<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>" href="#" onclick="{{ $funcion_carrito }}" >
            <span class='hidden-xs'>Carrito</span>
            <span id='carrito-compra' class="glyphicon glyphicon-shopping-cart">0</span>

        </a>
    </li>
    <li class="visible-xs">
        <a href='#'  data-toggle="offcanvas">
            <span class="glyphicon glyphicon-menu-right"></span>
        </a>
    </li>
    
</ul>