
    @foreach($facetas as $nombre => $valores)
    <div>
    <a data-toggle="collapse" href="#{{ camel_case($nombre) }}" aria-expanded="false" aria-controls="{{ camel_case($nombre) }}">
        <strong class="text-capitalize">Por {{ $nombre }} </strong>
    </a>
    
    <ul class='list-unstyled collapse' id="{{ camel_case($nombre) }}">
        <li>
            <ul class=" leamas">
            @foreach($valores as $value => $count)
                @if($count>10)
                    <li>
                        <a id='agregar-faceta-{{ camel_case($nombre) }}' href='#' onclick="agregarFaceta('{{ strtolower($nombre) }}','{{ $value }}')" >
                            <span class='small'>
                                {{ $value }}
                            </span>

                            <span class='small'>
                                ({{ $count }})
                            </span>
                        </a>
                    </li>
                @endif
            @endforeach
            
            </ul>
        </li>
    </ul>
    </div>
    @endforeach


<script>
    jQuery('.leamas').readmore({
                speed: 100,
                collapsedHeight: 200,
                heightMargin: 10,
                moreLink: '<a  href="#">Mostrar más</a>',
                lessLink: '<a  href="#">Mostrar menos</a>',
                embedCSS: true,
                blockCSS: 'display: block; width: 100%;',
                startOpen: false,

                // callbacks
                beforeToggle: function(){},
                afterToggle: function(){}
            });
      
</script>