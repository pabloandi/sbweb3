@extends('opac')

@section('content')



            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" >
                <div class="container">
                    <div class="navbar-header">
                         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#encabezado-otras-opciones" aria-expanded='false' aria-controls='encabezado-otras-opciones'>
                            <span class="sr-only">...</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            
                        </button>
                    <div class='navbar-brand'>@include('opac.partials._botonera_superior')</div>    
                    </div>
                    
                    <div class="collapse navbar-collapse" id="encabezado-otras-opciones">
                        
                        <div class="form-group ">
                            
                            @include('opac.partials._consulta_perfil')
                        </div>
                </div>
            </nav> {{-- Termina encabezado del panel --}}

            
    
        <div class="container" id='main'>
            <div class="row row-offcanvas row-offcanvas-right">
                @include('opac.partials._cuerpo')
                
                {{-- Empieza sidebar --}}
            
                @include('opac.partials._sidebar')
            
                {{-- Termina sidebar --}}
                
            </div>
                {{-- Empieza footer --}}
                <footer>
                        &copy; 2014 by <a href="http://www.cali.gov.co/redbibliotecas/" target="_blank">Red de Bibliotecas Públicas Comunitarias de Cali</a> Todos los derechos reservados.
                        Impulsado por <a href="http://www.sib.com.co" target="_blank">S.I.B</a>                    
                </footer>
                {{-- Termina footer --}}
            
            
        </div> {{-- Termina cuerpo del panel --}}
        
        
     {{-- Termina panel principal --}}
        


    

@stop
