{{
    Form::jqRemoteForm(array(
                                        'update'    =>  'respuesta_contacto',
                                        'url'       =>  URL::action('OPACController@getContacto'),
                                        'condition' =>  "jQuery('#nombre_usuario_contacto').val()!='' && jQuery('#correo_usuario_contacto').val()!='' && jQuery('#mensaje_usuario_contacto').val()!=''",
                                        'loading'   =>  "jQuery('#contacto-inst-loader').show()",

                                        'success'   =>  'bootbox.alert(data);',
                                        'method'    =>  'post',
                                        'with'      =>  "jQuery('#form-contacto-institucion').serialize()"
    ),
    array(
        'id'    =>  'form-contacto-institucion',
        'role'  =>  'form'
    )) 
}}

<form id="form-contacto_institucion" role="form">
        <div class="form-group">
            <label for="nombre_usuario_contacto">Nombre del usuario</label>
            <input type="text" class="form-control" id="nombre_usuario_contacto" placeholder="digite el nombre"/>
        </div>
        <div class="form-group">
            <label for="correo_usuario_contacto">Correo del usuario</label>
            <input type="email" class="form-control" id="correo_usuario_contacto" placeholder="digite el correo"/>
        </div>
        <div class="form-group">
            <label for="plan_usuario_contacto">Asunto del usuario</label>
            <input type="text" class="form-control" id="plan_usuario_contacto" placeholder="digite el asunto"/>
        </div>
        <div class="form-group">
            <label for="biblioteca_contacto">Biblioteca</label>
            <select name="biblioteca_contacto" class="form-control" id="biblioteca_contacto">
            @foreach($bibliotecas as $biblioteca)
                <option value="{{ $biblioteca->idbiblioteca }}"> {{ $biblioteca->nombre }} </option>
            @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="mensaje_usuario_contacto">Mensaje</label>
            <textarea class="form-control" id="mensaje_usuario_contacto" name="mensaje_usuario_contacto" cols="40" rows="10" placeholder="digite el mensaje"/></textarea>
        </div>
        
    
    </form>
