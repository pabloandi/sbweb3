<div class="list-group" id="acordeon-fichasejemplares">
    
    @foreach($fichas as $ficha)
    <a data-toggle='collapse' data-parent='acordeon-fichasejemplares' href="#ficha-{{ $ficha->idficha }}" class="list-group-item">
        <h4 class="list-group-item-heading">{{ $ficha->titulo }}</h4>
        <p class="list-group-item-text">{{ $ficha->clasificacion }}</p>
    </a>
    <div id="ficha-{{ $ficha->idficha }}" class="collapse">
        <div id="ficha-detalle">
            @include('opac.partials.fichas._ficha_detalle',array('etiquetasmarc'=>$ficha->procesarEtiquetasmarc()))
        </div>
        <div id='ejemplares'>
            @include('opac.partials.fichas._ficha_ejemplares',array('ejemplares'=>$ficha->ejemplares()->get()) )
        </div>
    </div>
    @endforeach
</div>