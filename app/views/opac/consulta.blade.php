<div >        
    <div id='paginacion' class="text-center">
        @include('pagination.slider',array('paginator'=>$pager))
    </div>
</div>
<div >
            <ul class="list-group">
                <li class='list-group-item'>
                    
                        <div class="btn-group">
                            <a class="btn btn-default" href="#" onclick="jQuery('.fila-ficha').each(function(i,e){jQuery(this).addClass('info')})" data-toggle="tooltip" data-placement="top" title="Seleccionar todas las fichas"><span class="glyphicon glyphicon-check"></span></a>
                            <a class="btn btn-default" href="#" onclick="jQuery('.fila-ficha').each(function(i,e){jQuery(this).removeClass('info')})" data-toggle="tooltip" data-placement="top" title="Deseleccionar todas las fichas"><span class="glyphicon glyphicon-unchecked"></span></a>
                        </div>
                    
                </li>
                
                @foreach($pager as $ficha)
                    @include('opac.partials.fichas._ficha_fila',array('ficha'=>$ficha))
                @endforeach
            </ul>
</div>
<div >
    
    <div id='paginacion' class="text-center">
        @include('pagination.slider',array('paginator'=>$pager))
    </div>
</div>
