<?php if ($paginator->getLastPage() > 1): ?>
    <ul class="pagination pagination-sm">
        <?php echo with(new Pagination\Presenters\SbwebPresenter($paginator))->render() ?>
    </ul>
<?php endif; ?>
