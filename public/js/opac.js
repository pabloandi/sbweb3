function agregarEjemplarCarro(){
    carro=jQuery("#carrito-compra");
    var cantidad=parseInt(carro.text());
    carro.text(cantidad+1);
}

function agregarFaceta(faceta,valor){
    
    jQuery("#filtro_facetas").append(
            '<input id="filtro-faceta-'+faceta.replace(/\s+/g, '')+'" type="hidden" name="filtros[AND][]" value="'+faceta.replace(/\s+/g, '')+':'+valor+'" />'
        );
    jQuery("#form_busqueda_principal").submit();
}

function removerFaceta(faceta){
    
    jQuery('#filtro-faceta-'+faceta.replace(/\s+/g, '')).remove();
    jQuery("#form_busqueda_principal").submit();
}

function actualizarFiltroAdicional(elemento){
    var operacion=jQuery(elemento).closest(".form-inline").find(".filtro-operacion :selected")[0].value;
    var categoria=jQuery(elemento).closest(".form-inline").find(".filtro-categoria :selected")[0].value;
    jQuery(elemento).closest(".form-inline").find(".filtro-caja")[0].name='filtros['+operacion+':'+categoria+'][]';
    
}

jQuery(function(){
        
         jQuery.ajaxSetup({
             cache: false
         });
         
        bootbox.setDefaults({
            locale: "es",
            closeButton: true,
        }); 
          
         $('.leamas').load(function(){
             $('.leamas').readmore({
                speed: 100,
                collapsedHeight: 200,
                heightMargin: 16,
                moreLink: '<a href="#">Lea más</a>',
                lessLink: '<a href="#">Cerrar</a>',
                embedCSS: true,
                blockCSS: 'display: block; width: 100%;',
                startOpen: false,

                // callbacks
                beforeToggle: function(){},
                afterToggle: function(){}
            });
         });

});